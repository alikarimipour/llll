package ir.dotin;

import ir.dotin.da.PersonDA;
import ir.dotin.entity.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Controller
public class WelcomeController {
    @Autowired
    PersonDA personDA;

    // inject via application.properties

    private String message = "Salam Donya";

    @RequestMapping("/slm")
    public ModelAndView welcome(Map<String, Object> model) {
        ModelAndView modelAndView = new ModelAndView("/WEB-INF/welcome.jsp");
        modelAndView.addObject("ali", this.message);
        Person person=new Person();
        person.setPersonName("sdf");
        personDA.save(person);
        return modelAndView;

    }

}