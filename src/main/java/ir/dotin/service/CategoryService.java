package ir.dotin.service;

import ir.dotin.da.CategoryDA;
import ir.dotin.entity.Category;
import ir.dotin.entity.OffRequest;
import ir.dotin.entity.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryService {
    @Autowired
    CategoryDA categoryDA;

    public Category loadCategoryById(Long categoryId) {
        Category categoryById = categoryDA.findCategoryById(categoryId);
        return categoryById;
    }
}
