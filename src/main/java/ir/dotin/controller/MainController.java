package ir.dotin.controller;

import ir.dotin.da.OffRequestDA;
import ir.dotin.entity.OffRequest;
import ir.dotin.entity.Person;
import ir.dotin.service.OffRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@Scope("request")
@RequestMapping()
public class MainController {

    @RequestMapping("/")
    public ModelAndView firstPage() {
        ModelAndView modelAndView = new ModelAndView("/person/findAll.do");
        modelAndView.addObject("c_active", true);
        return modelAndView;
    }

}
