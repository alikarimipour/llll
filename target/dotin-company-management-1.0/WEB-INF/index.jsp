<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>FIND ALL PAGE</title>
</head>
<a href="/person/savePage.do" >Go To Save Page</a>
<br/>
<form action="/person/findAll.do">
    <table border="1">
        <tr>
            <td>Show Only Active Persons</td>
            <td><input type="checkbox" name="c_active" checked/></td>
        </tr>
        <tr>
            <td>
                <input type="submit" value="Show"/>
            </td>
        </tr>
    </table>
</form>

<table border="1" style="width: 100%">

    <tr>
        <td>personName</td>
        <td>personFamily</td>
        <td>personnelCode</td>
        <td>nationalCode</td>
        <td>personPhone</td>
        <td>UPDATE</td>
        <td>CHANGE STATUS</td>
        <td>OFF REQUESTS</td>
    </tr>

    <c:forEach items="${persons}" var="person">
        <tr>
            <td>${person.personName}</td>
            <td>${person.personFamily}</td>
            <td>${person.personnelCode}</td>
            <td>${person.nationalCode}</td>
            <td>${person.personPhone}</td>
            <td><a href="/person/update.do?c_ID=${person.c_ID}">update</a></td>
            <c:if test="${person.c_active != true}">
                <td><a href="/person/active.do?c_ID=${person.c_ID}&c_active=false">make active</a></td>

            </c:if>
            <c:if test="${person.c_active == true}">
                <td><a href="/person/deactivate.do?c_ID=${person.c_ID}&c_active=true">make deactivate</a></td>
            </c:if>
            <td><a href="/offRequest/offRequest.do?c_ID=${person.c_ID}&personName=${person.personName}">offRequest</a>
            </td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
