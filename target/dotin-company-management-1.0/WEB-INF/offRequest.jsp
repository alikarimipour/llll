<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="C" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>FIND ALL PAGE</title>
</head>
<a href="/person/findAll.do">Go To FindAll Page</a>
<br/>
<form action="/offRequest/saveOffRequest.do" method="post">
    <input type="hidden" name="requesterPerson.c_ID" value="${person.c_ID}"/>
    <input type="text" name="offDescription" value="Description"/>
    <input type="text" name="offStartDate" value="Start Date"/>
    <input type="text" name="offEndDate" value="End Date"/>
    <select name="typeOfRequest.c_ID">
        <C:forEach items="${offCategory.subCategories}" var="offSub">
            <option value="${offSub.c_ID}">${offSub.subCategoryFarsiName}</option>
        </C:forEach>
    </select>
    <input type="submit">
</form>

<table border="1" style="width: 100%">

    <thead style="background-color: gray;color: aliceblue">
    <td>personName</td>
    <td>personFamily</td>
    <td>personnelCode</td>
    <td>nationalCode</td>
    <td>personPhone</td>
    </thead>

    <tr>
        <td>${person.personName}</td>
        <td>${person.personFamily}</td>
        <td>${person.personnelCode}</td>
        <td>${person.nationalCode}</td>
        <td>${person.personPhone}</td>
        </td>
    </tr>
</table>

<table border="1" style=" color: darkblue; width: 100%">
    <thead style=" color: darkblue">
    <td>personName</td>
    <td>typeOfRequest</td>
    <td>receiverManagerPersonName</td>
    <td>offDescription</td>
    <td>creationDate</td>
    <td>offStartDate</td>
    <td>offEndDate</td>
    </thead>

    <c:forEach items="${person.offRequestList}" var="offrequest">
        <tr>
            <td>${offrequest.requesterPerson.personName}</td>
            <td>${offrequest.typeOfRequest.subCategoryFarsiName}</td>
            <td>${offrequest.receiverManagerPerson.personName}</td>
            <td>${offrequest.offDescription}</td>
            <td>${offrequest.creationDate}</td>
            <td>${offrequest.offStartDate}</td>
            <td>${offrequest.offEndDate}</td>
        </tr>
    </c:forEach>

</table>
</body>
</html>
